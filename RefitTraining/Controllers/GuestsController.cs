﻿using Microsoft.AspNetCore.Mvc;
using RefitTraining.Models;
using System.Reflection;

namespace RefitTraining.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class GuestsController : ControllerBase
	{

		private static List<GuestModel> guests = new List<GuestModel>()
		{
			new GuestModel() { Id = 1, FirstName = "Max", LastName = "Maximov"},
			new GuestModel() { Id = 2, FirstName = "Mark", LastName = "Omegov"},
			new GuestModel() { Id = 2, FirstName = "Jackie", LastName = "Chan"},
		};

		[HttpGet]
		public IEnumerable<GuestModel> Get()
		{
			return guests;
		}

		[HttpGet("{id}")]
		public GuestModel Get(int id)
		{
			return guests.Where(g => g.Id == id).FirstOrDefault();
		}

		[HttpPost]
		public void Post([FromBody] GuestModel model)
		{
			guests.Add(model);
		}

		[HttpPut("{id}")]
		public void Put(int id, [FromBody] GuestModel model)
		{
			guests.Remove(guests.Where(g => g.Id == model.Id).FirstOrDefault());
			guests.Add(model);
		}

		[HttpDelete("{id}")]
		public void Delete(int id)
		{
			guests.Remove(guests.Where(g => g.Id == id).FirstOrDefault());

		}
	}
}
