using Microsoft.AspNetCore.Mvc;
using RefitAPI.DataAccess;

namespace RefitAPI.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class WeatherForecastController : ControllerBase
	{
		private static readonly string[] Summaries = new[]
		{
				"Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
		};

		private readonly ILogger<WeatherForecastController> _logger;

		private readonly IGuestData _guestData;


		public WeatherForecastController(ILogger<WeatherForecastController> logger,
			IGuestData guestData)
		{
			_logger = logger;
			_guestData = guestData;
		}

		[HttpGet(Name = "GetWeatherForecast")]
		public async Task<IEnumerable<GuestModel>> Get()
		{
			return await _guestData.GetGuests();
			//return Enumerable.Range(1, 5).Select(index => new WeatherForecast
			//{
			//	Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
			//	TemperatureC = Random.Shared.Next(-20, 55),
			//	Summary = Summaries[Random.Shared.Next(Summaries.Length)]
			//})
			//.ToArray();
		}
	}
}