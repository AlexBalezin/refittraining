using Refit;
using RefitAPI.DataAccess;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var httpClientHandler = new HttpClientHandler
{
	SslProtocols = System.Security.Authentication.SslProtocols.Tls12,
	ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true
};

builder.Services.AddRefitClient<IGuestData>().ConfigureHttpClient(c =>
{
	c.BaseAddress = new Uri("https://localhost:7184/api");
})
.ConfigurePrimaryHttpMessageHandler(() => httpClientHandler);

builder.Services.AddCors(policy =>
{
	policy.AddPolicy("OpenCorsPolicy", opt =>
	{
		opt.AllowAnyOrigin()
			.AllowAnyHeader()
			.AllowAnyMethod();
	});
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
	app.UseSwagger();
	app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors("OpenCorsPolicy");

app.UseAuthorization();

app.MapControllers();

app.Run();
